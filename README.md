# AgileIT Report Angular App

Esse projeto foi gerado com a versão `6.0.8` do Angular CLI

## Compilação

Para compilar a aplicação para execução em um ambiente local, execute o comando:
- `ng build`

Para o ambiente de produção, ou seja, para fazer o deploy, execute  o comando:
- `ng build --prod`

## Executar aplicação local

Execute o comando `ng server` no terminal, dentro da pasta do projeto.
Depois, navegue até o endereço `http://localhost:4200`. Qualquer alteração no projeto durante a execução, irá recompilar automaticamente e as mudanças serão refletidas imediatamente no navegador, sem a necessidade de atualizar a página.

