import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import $ from "jquery";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


/**
 * @description Objeto de entidade do Token de acesso ao Report Server 
 */
interface ReportServerAccessToken {
	access_token: string;
	token_type: string;
	expires_in: number;
	userName: string;
	'.expires': string;
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})


export class AppComponent {
	@ViewChild('viewer1') viewer;
	sessionTokenKey: string = 'ReportAccessToken';
	baseReportUrl: string = 'https://demos.telerik.com/report-server/';
	baseApiUrl: string = this.baseReportUrl + 'api/reportserver/';
	accessTokenSessionKey: string = 'ReportServerAccessToken';
	login: string = 'demouser';
	senha: string = 'demopass';
	token: ReportServerAccessToken;
	clientId: string;
	reports: Object;
	report: Object;
	reportParameters: Object;
	reportCategory: Object;
	showReport: Boolean = false;
	viewerContainerStyle = {
		position: 'relative',
		width: '1000px',
		height: '800px',
		['font-family']: 'ms sans serif'
	};
	closeResult: string;
	filter: string;
	loading = false;

	constructor(private httpClient: HttpClient, private modalService: NgbModal) {
		
	}


    /**
	 * @description Método que limpa o objeto de sessão.
	 */
	public CleanSession() {
		window.sessionStorage.removeItem(this.accessTokenSessionKey);
	}



	/**
	 * @description Método que obtém o Token de acesso do Report Server
	 * @returns Objeto Promise   	
	 */
	GetReportToken() {
		let promise = new Promise((resolve, reject) => {
			if (!this.ValidateAccessToken()) {
				this.LogInReport().then(() => {
					resolve();
				});
			}
			else {
				resolve();
				return this.token.access_token;
			}
		});

		return promise;
	}



	/**
	 * @description Método que obtém a lista de relatórios existentes no servidor   	
	 */
	GetReports() {
		this.loading = true;
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();

		this.GetReportToken().then(() => {
			const httpOptions = {
				headers: httpHeaders.set('Authorization', 'Bearer ' + this.token.access_token)
			};
			this.httpClient.get(this.baseApiUrl + "reports", httpOptions)
				.subscribe(data => {
					this.loading = false;
					this.reports = data;
				});
		});
	}



	/**
	 * @description Método que obtém a categoria de um relatório específico 
	 * @returns Objeto Promise 	
	 */
	GetReportCategory(categoryId) {
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();
		const httpOptions = {
			headers: httpHeaders.set('Authorization', 'Bearer ' + this.token.access_token)
		};

		let promise = new Promise((resolve, reject) => {
			this.httpClient.get(this.baseApiUrl + "categories/" + categoryId, httpOptions)
				.toPromise()
				.then(data => {
					this.reportCategory = data;
					resolve();
				});
		});
		
		return promise;
	}
	


	/**
	 * @description Método que carrega o relatório selecionado dentro 
	 * 				do Report Viewer e abre um modal de apresentação.   	
	 */
	ShowReport(content, report) {	
		this.loading = true;
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();	
		
		this.GetReportCategory(report.CategoryId).then(() => {
			report.Category = this.reportCategory; 
			this.report = report;
			
			let reportName = report.Category.Name + "/" + report.Name + report.Extension;
			this.GetClientId().then(clientId => {
				this.GetReportParameters(clientId, reportName).then(parameters => {
					this.loading = false;
					report.Parameters = parameters;
				});
			})

			this.showReport = true;
			this.modalService.open(content, { size: 'lg' });
		});
	}


	/**
	 * @description Método para a geração dinãmica de componentes de parâmetros.   
	 * @param Object Recebe um objeto contendo um parâmetro do relatório.	
	 */
	CreateFilter(parameter) {
		let filter = '';

		//Cria um dropdown e carrega os valores do diltro
		if(parameter.availableValues !== null) {
			filter = '<select id="'+ parameter.id +'">';
			parameter.availableValues.forEach(param => {
				filter += '<option value="'+ param.value +'">'+ param.name +'</option>' 
			});
			
			filter += '</select>';
		}
		this.filter = filter;
	} 
	


	/**
	 * @description Método que registra um ClientId para um relatório.
	 * @returns Objeto Promise
	 */
	GetClientId() {
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();
		const httpOptions = {
			headers: httpHeaders.set('Authorization', 'Bearer ' + this.token.access_token)
		};

		let promise = new Promise((resolve, reject) => {
			this.httpClient.post(this.baseReportUrl + "api/reports/clients", null, httpOptions)
				.toPromise()
				.then(data => {
					resolve(data['clientId']);
				});
		});

		return promise;
	}



	/**
	 * @description Método que obtém a lista de parâmetros de um relatório  
	 * @param String ClientId
	 * @param String Nome do relatório, composto por: <Categoria>/<Nome>.<Extensão>
	 * 		   	
	 * @returns Objeto Promise
	 */
	GetReportParameters(clientId, reportName) {
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();
		const httpOptions = {
			headers: httpHeaders.set('Authorization', 'Bearer ' + this.token.access_token)
		};

		let paramz = httpParams
			.append("report", reportName)
			.append("parameterValues", '{}')

		let promise = new Promise((resolve, reject) => {
			this.httpClient.post(this.baseReportUrl + "api/reports/clients/" + clientId + "/parameters", paramz, httpOptions)
				.toPromise()
				.then(parameters => {
					resolve(parameters);
				});
		});

		return promise;
	}



	/**
	 * @description Método que efetua o LogIn no Report Server, e armazena o Token gerado.
	 * @returns Objeto Promise
	 */
	LogInReport() {
		let httpParams = new HttpParams();
		let httpHeaders = new HttpHeaders();
		const httpOptions = {
			headers: httpHeaders.set('Content-Type', 'application/x-www-form-urlencoded')
		};

		let paramz = httpParams
			.append("grant_type", "password")
			.append("username", this.login)
			.append("password", this.senha);

		let promise = new Promise((resolve, reject) => {
			this.httpClient.post<ReportServerAccessToken>(this.baseReportUrl + "Token", paramz, httpOptions)
				.toPromise()
				.then(data => {
					this.token = data;
					window.sessionStorage.setItem(this.accessTokenSessionKey, JSON.stringify(data));
					resolve();
				});
		});

		return promise;
	}



	/**
	 * @description Método que verifica a validade e a integridade do Token de acesso.     	
	 */
	private ValidateAccessToken() {
		let accessTokenObj = JSON.parse(window.sessionStorage.getItem(this.accessTokenSessionKey));

		if (accessTokenObj !== null) {
			this.token = <ReportServerAccessToken>accessTokenObj;

			let start_date = new Date(this.token['.issued']).getTime();
			let end_date = new Date(this.token['.expires']).getTime();

			if (this.token === null)
				return false;

			if (end_date <= Date.now()) {
				this.CleanSession();
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return false;
		}
	}
}