import { BrowserModule } from '@angular/platform-browser';  
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { TelerikReportingModule } from '@progress/telerik-angular-report-viewer';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingModule } from 'ngx-loading';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TelerikReportingModule,
    NgbModule.forRoot(),
    //LoadingBarHttpClientModule,
    LoadingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }